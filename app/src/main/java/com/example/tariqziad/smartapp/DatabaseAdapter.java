package com.example.tariqziad.smartapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tariq ziad on 11/12/2016.
 */

public class DatabaseAdapter {

    DatabaseHelper helper;
    public DatabaseAdapter(Context context){

        helper = new DatabaseHelper(context);
    }

    public long insertData(String name, String password)
    {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.NAME,name);
        contentValues.put(DatabaseHelper.PASSWORD,password);
       long id =  db.insert(DatabaseHelper.TABLE_NAME,null,contentValues);
        db.close();
        return id;
    }

    public String getAllData()
    {
        SQLiteDatabase db = helper.getWritableDatabase();
        String[] columns ={DatabaseHelper.UID, DatabaseHelper.NAME, DatabaseHelper.PASSWORD};

        Cursor cursor = db.query(DatabaseHelper.TABLE_NAME, columns,null,null,null,null,null);
        StringBuffer buffer = new StringBuffer();

        while (cursor.moveToNext()){

            int cid = cursor.getInt(0);
            String name = cursor.getString(1);
            String password = cursor.getString(2);
            buffer.append(cid+" "+name+" "+password);
        }
        return buffer.toString();
    }

    static class DatabaseHelper extends SQLiteOpenHelper{

        private static final String DATABASE_NAME= "MyDatabase";
        private static final String TABLE_NAME= "User";
        private static final int DATABASE_VERSION= 4;
        private static final String UID= "_id";
        private static final String NAME = "NAME";
        private static final String PASSWORD = "Password";
        private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+NAME+" VARCHAR(225)"+PASSWORD+" VARCHAR(225));";
        private static final String DROP_TABLE = "DROP TABLE IF EXISTS"+TABLE_NAME;

        private Context context;


        public DatabaseHelper(Context context){

            super(context,DATABASE_NAME,null,DATABASE_VERSION);
            this.context= context;
            Message.message(context,"Constructor Called");
        }

        @Override

        public void onCreate(SQLiteDatabase db)
        {
            try {
                db.execSQL(CREATE_TABLE);
                Message.message(context,"onCreate Called");
            }catch (SQLiteException e){
                Message.message(context,""+e);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int OldVersion, int NewVersion) {
            try {
                Message.message(context,"onUpgrade Called");
                db.execSQL(DROP_TABLE);
                onCreate(db);
            } catch (SQLiteException e){
                Message.message(context,""+e);
            }
        }

    }
}
