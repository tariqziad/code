package com.example.tariqziad.smartapp;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText userName, password;

    DatabaseAdapter databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userName = (EditText) findViewById(R.id.editTextUser);
        password = (EditText) findViewById(R.id.editTextPass);
        databaseHelper = new DatabaseAdapter(this);

    }
    public void addUser(View view){

        String user=userName.getText().toString();
        String pass= password.getText().toString();

        long id = databaseHelper.insertData(user,pass);
        if(id<0){
            Message.message(this,"Unsuccessful");

        }else{

            Message.message(this,"Successful");
        }
    }

     public void viewDetails(View view)
    {
        String data = databaseHelper.getAllData();
        Message.message(this,data);
    }

}
